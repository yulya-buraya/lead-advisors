import getZero from "./getZero";

export default function getTimeRemaining(deadline) {
	const t = Date.parse(deadline) - new Date(),
		days = Math.floor(t / (1000 * 60 * 60 * 24)),
		hours = Math.floor((t / (1000 * 60 * 60)) % 24),
		minutes = Math.floor((t / (1000 * 60)) % 60),
		seconds = Math.floor((t / 1000) % 60);
	return {
		total: t,
		days: getZero(days),
		hours: getZero(hours),
		minutes: getZero(minutes),
		seconds: getZero(seconds),
	};
}

