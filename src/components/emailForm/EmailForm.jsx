import arrowRight from "../../svg/arrowRight.svg";
import "./emailForm.css";
import "../../App.css";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useState } from "react";
import Modal from "../modal/Modal";
import ModalPortal from "../modalPortal/ModalPortal";


const EmailForm = () => {
	const [isModalActive, setModalActive] = useState(false);
	const [requestStatus, setRequestStatus] = useState("");
	const [requestMessage, setRequestMessage] = useState("");
	const formik = useFormik({
		initialValues: {
			email: ""
		},
		validationSchema:Yup.object({
			email: Yup.string()
				.email("Invalid email address")
				.required("Required field!")
		}),
		onSubmit: (values, { setSubmitting, resetForm }) => {
			submitData(values, setSubmitting, resetForm);
		}
	});

	function submitData(values, setSubmitting, resetForm) {
		const request = new XMLHttpRequest();
		request.open("POST", "https://eo8dc5hyzjq4im2.m.pipedream.net");
		request.setRequestHeader('Content-type', "application/json");

		request.onload = () => {
			if (request.status >= 200 && request.status < 300) {
				setRequestStatus("SUCCESS!");
				setRequestMessage("You have successfully subscribed to the email newsletter");
                setModalActive(true);
				resetForm();
			} else {
				setRequestStatus("ERROR!");
				setRequestMessage("Something went wrong, please try again later")
				setModalActive(true);
			}
			setSubmitting(false);
		};

		request.onerror = (e) => {
			setRequestStatus("ERROR!");
			setRequestMessage(e.toString())
			setModalActive(true);
			setSubmitting(false);
		};
		request.send(JSON.stringify({ email: values.email }));
	}


	return (<>
			<form onSubmit={ formik.handleSubmit } className="form">
				<div className="input-container" >
					<input type="email"
						   name="email"
						   id="email"
						   className="input-field"
						   placeholder="Enter your Email and get notified"
						   value={ formik.values.email }
						   onChange={ formik.handleChange }
						   onBlur={formik.handleBlur}
						   required
					/>
					<button type="submit" className="input-button">
						<img className="arrow-right-icon" src={ arrowRight } alt="send"/>
					</button>
				</div>
				{formik.errors.email && <formik className="touched email"></formik>? <div className="error">{formik.errors.email}</div>:null}
			</form>
			<ModalPortal>
				<Modal active={isModalActive}
					   setActive={setModalActive}
					   title={requestStatus}
					   message={requestMessage}/>
			</ModalPortal>
	</>

)
}

export default EmailForm;