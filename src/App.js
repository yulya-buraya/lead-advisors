import './App.css';
import RightCorner from "./svg/rightCorner.svg";
import LeftCorner from "./svg/leftCorner.svg";
import logo from "./svg/logo.svg";
import arrowRight from "./svg/arrowRight.svg"
import arrowDown from "./svg/arrowDown.svg"
import TimerComponent from "./components/timer/Timer";
import EmailForm from "./components/emailForm/EmailForm";
import HorizontalAccordion from "./components/horizontalAccordion/HorizontalAccordion";
import SOCIAL_EVENTS from "./socialEvents.json";

function App() {
	return (
		<div className="main-container">
			<img className="left-top-figure" src={ LeftCorner } alt="left-corner"/>
			<img className="right-top-figure" src={ RightCorner } alt="right-corner"/>
			<div className="main">
				<a href="/lead-advisors">
					<img className="logo" alt="logo" src={ logo }/>
				</a>
				<h1 className="main title">under construction</h1>
				<div className="main subtitle"> We're making lots of improvements and will be back soon</div>
				<TimerComponent/>
				<div className="event-block">
					<div className="event-page-message"> Check our event page when you wait:</div>
					<a href="https://egorovagency.com/"
					   className="go-the-event-button"
					   target="_blank"
					   rel="noreferrer"
					>
						<span>Go to the event</span>
						<img className="arrow-right-icon" src={ arrowRight } alt=''/>
					</a>
				</div>
			</div>
			<div className="second-block">
				<EmailForm/>
				<a className="button-other" href="#accordion-block">
					<span>Other Events</span>
					<img src={ arrowDown } alt=""/>
				</a>
			</div>
			<div className={ "accordion-container" } id="accordion-block">
				<div className="title"> all events</div>
				<div className="accordion" id="accordion">
					<HorizontalAccordion events={ SOCIAL_EVENTS }/>
				</div>
			</div>
		</div>

	);
}

export default App;
