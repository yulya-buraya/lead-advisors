import "./timer.css";
import label from "../../svg/label.svg";
import { useEffect, useState } from "react";
import getTimeRemaining from "../../utils/getTimeRemaining";

const TimerComponent = () => {
	const screenWidth = window.innerWidth;
	const [screenType, setScreenType] = useState(null);
	const DEADLINE_TIME = "2024-07-24 00:00:00";
	const initial = getTimeRemaining(DEADLINE_TIME);
	const [ [ days, hours, minutes, seconds ], setTime ] = useState([
		initial.days,
		initial.hours,
		initial.minutes,
		initial.seconds
	]);

	useEffect(() => {
		if (screenWidth > 1500) {
			setScreenType("desktop");
		} else if (screenWidth >= 600 && screenWidth < 1024) {
			setScreenType("tablet");
		} else {
			setScreenType('mobile');
		}
	},[screenWidth]);

	useEffect(() => {
		const timerID = setInterval(() => {
			const t = getTimeRemaining(DEADLINE_TIME);
			setTime([ t.days, t.hours, t.minutes, t.seconds ])
		}, 1000);
		return () => clearInterval(timerID);
	}, [ days, hours, minutes, seconds ])

	return (
		<div className="timer">
			<div className="timer-item">
				<div className="timer-item-value">{ days }</div>
				<div className="timer-label-container">
					<img className="timer-label" src={ label } alt="label"/>
					<div className="label-text">{ screenType==="desktop"? "Days":"DD" }</div>
				</div>
			</div>
			<div className="timer-separator">:</div>
			<div className="timer-item">
				<div className="timer-item-value">{ hours }</div>
				<div className="timer-label-container">
					<img className="timer-label" src={ label } alt="label"/>
					<div className="label-text">{ screenType==="desktop"? "Hours":"HH" }</div>
				</div>
			</div>
			<div className="timer-separator">:</div>
			<div className="timer-item">
				<div className="timer-item-value">{ minutes }</div>
				<div className="timer-label-container">
					<img className="timer-label" src={ label } alt="label"/>
					<div className="label-text">{ screenType==="desktop"? "Minutes":"MM" }</div>
				</div>
			</div>
			<div className="timer-separator">:</div>
			<div className="timer-item">
				<div className="timer-item-value">{ seconds }</div>
				<div className="timer-label-container">
					<img className="timer-label" src={ label } alt="label"/>
					<div className="label-text">{ screenType==="desktop"? "Seconds":"SS" }</div>
				</div>
			</div>
		</div>
	)
}

export default TimerComponent;