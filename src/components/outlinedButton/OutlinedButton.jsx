import './OutlinedButton.css'

function OutlinedButton({ text, style }) {
    return (
        <a className="more-info-button-wrapper" style={style} href="https://egorovagency.com/">
            <span>{text}</span>
        </a>
    );
}

export default OutlinedButton
