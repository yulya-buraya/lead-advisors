import "./modal.css";
import cancelIcon from "../../svg/cancelcon.svg"

const Modal = ({ active, setActive, message, title }) => {

	const handleCancel = () => {
		setActive(false);
	}

	return (
		<div className={ active ? 'modal active' : 'modal' } onClick={ handleCancel }>
			<div className='modal-block' onClick={ e => e.stopPropagation() }>
				<div className="close-icon-block" >
					<img src={ cancelIcon } alt="X" onClick={ handleCancel }/>
				</div>
				<div className="modal-content">
					<h1 className="modal-title">{ title }</h1>
					<div className='modal-message'>{ message }</div>
				</div>
				<div className='modal-cancel-btn' onClick={ handleCancel }>Close</div>
			</div>
		</div>
	);
}

export default Modal;